#!/usr/bin/env python3

import os

class HeadTail:
    _moves = {
        "U": (0,1),
        "D": (0,-1),
        "L": (-1,0),
        "R": (1,0)
    }

    def __init__(self):
        self.hx = 0
        self.hy = 0
        self.tx = 0
        self.ty = 0
    
    def move(self, direction):
        self.hx = self.hx + HeadTail._moves[direction][0]
        self.hy = self.hy + HeadTail._moves[direction][1]

    def move_by(self, x, y):
        self.hx = self.hx + x
        self.hy = self.hy + y

    def move_tail(self): 
        difference = (self.hx - self.tx, self.hy - self.ty)
        abs_difference = (abs(difference[0]), abs(difference[1]))
        if max(abs_difference) <= 1:
            return 0,0
        if sum(abs_difference) == 4:
            self.tx = self.tx + (difference[0] // 2)
            self.ty = self.ty + (difference[1] // 2)
            return (difference[0] // 2), (difference[1] // 2)
        if sum(abs_difference) == 2:
            if abs_difference[0] == 0:
                self.ty = self.ty + (difference[1] // 2)
                return 0, (difference[1] // 2)
            else:
                self.tx = self.tx + (difference[0] // 2)
                return (difference[0] // 2), 0
        if abs_difference[0] == 2:
            self.tx = self.tx + (difference[0] // 2)
            self.ty = self.ty + difference[1]
            return (difference[0] // 2), difference[1]
        else:
            self.tx = self.tx + difference[0]
            self.ty = self.ty + (difference[1] // 2)
            return difference[0], (difference[1] // 2)

    @property
    def head(self):
        return self.hx, self.hy

    @property
    def tail(self):
        return self.tx, self.ty

def render(knots):
    max_x = max(max([knot.hx for knot in knots]), 10)
    min_x = min(min([knot.hx for knot in knots]), -10)
    max_y = max(max([knot.hy for knot in knots]), 3)
    min_y = min(min([knot.hy for knot in knots]), -3)
    grid = [["." for _ in range(min_x, max_x + 1)] for _ in range(min_y, max_y + 1)]
    grid[0-min_y][0-min_x] = "s"
    grid[knots[8].ty-min_y][knots[8].tx-min_x] = "9"
    for ix in range(8, -1, -1):
        knot = knots[ix]
        grid[knot.hy-min_y][knot.hx-min_x] = "H" if ix == 0 else f"{ix}"    

    for row in grid[::-1]:
        print("".join(row))

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    tail_visited = set({})

    knots = [HeadTail() for _ in range(9)]
    for line in data:
        direction = line[0]
        distance = int(line[1:])
        for dc in range(distance):
            knots[0].move(direction)
            for ix in range(8):
                tail_move = knots[ix].move_tail()
                knots[ix+1].move_by(tail_move[0], tail_move[1]) 
            knots[8].move_tail()
            tail_visited.add(knots[8].tail)

    answer = len(tail_visited)

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
